variable "docker_base_image" { type=string }
variable "docker_tag" { type=string }
variable "docker_repository" { type=string }


build {
  name = "gitlab-runner"

  source "source.docker.alpine" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
      "ENTRYPOINT [\"\"]",
      "CMD []"
    ])
  }

  provisioner "shell" {
    inline = [
      "apk -U upgrade",
      "apk -v cache clean || true",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add cargo curl git gcc g++ libressl libxml2-dev libxslt-dev make rust",
      "apk add build-base"
    ]
    only = ["docker.alpine"]
  }

  provisioner "shell" {
    inline = [
      "pip install pip --upgrade"
    ]
  }

  source "source.docker.debian" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
      "ENTRYPOINT [\"\"]",
      "CMD []"
    ])
  }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt-get update",
      "apt-get upgrade -y",
      "apt-get install -y curl git gcc g++ libxml2-dev libxslt-dev openssl make"
    ]
    only = ["docker.debian"]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = var.docker_repository
      tags        = [var.docker_tag]
    }

    post-processor "docker-push" {}
  }
}


build {
  name = "runtime"

  source "source.docker.alpine" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
    ], local.docker_env)
  }

  source "source.docker.debian" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
    ], local.docker_env)
  }

  provisioner "shell" {
    inline = [
      "apk -U upgrade",
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "apk -v cache clean || true",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add nginx gettext libressl",
      "echo 'apk add $@' > /usr/local/bin/pkg-add",
      "echo 'apk update' > /usr/local/bin/pkg-update"
    ]
    only = ["docker.alpine"]
  }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt-get update",
      "apt-get upgrade -y",
      "addgroup --gid ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} --uid ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/bash ${local.runtime_usr} --disabled-password --gecos \"\"",
      "apt-get install -y nginx gettext openssl",
      "echo 'apt-get install -y $@' > /usr/local/bin/pkg-add",
      "echo 'apt-get update' > /usr/local/bin/pkg-update"
    ]
    only = ["docker.debian"]
  }

  provisioner "shell" {
    inline = [
      "mkdir -p ${local.app_secdir}",
      "mkdir -p ${local.app_secdir}/rdbms",
      "mkdir -p ${local.app_secdir}/cache",
      "chmod +x /usr/local/bin/pkg-add",
      "chmod +x /usr/local/bin/pkg-update"
    ]
  }

  provisioner "file"{
    source = "files/etc/nginx/nginx.conf"
    destination = "/etc/nginx/nginx.conf"
  }

  #provisioner "file"{
  #  source = "files/bin/docker-entrypoint.d/${var.language}"
  #  destination = "/usr/local/bin/docker-entrypoint"
  #}

  post-processors {
    post-processor "docker-tag" {
      repository  = var.docker_repository
      tags        = [var.docker_tag]
    }

    post-processor "docker-push" {}
  }
}


build {
  name = "build"

  source "source.docker.alpine" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
      "WORKDIR /build",
      "ENV PIP_DISABLE_PIP_VERSION_CHECK 1"
    ])
  }

  source "source.docker.debian" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
      "WORKDIR /build",
      "ENV PIP_DISABLE_PIP_VERSION_CHECK 1"
    ])
  }

  provisioner "shell" {
    inline = [
      "apk -U upgrade",
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "apk -v cache clean || true",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add cargo curl gcc g++ git go make libffi-dev libressl-dev libxml2-dev libxslt-dev rust",
      "apk add build-base",
      "echo 'apk add $@' > /usr/local/bin/pkg-add",
      "echo 'apk update' > /usr/local/bin/pkg-update"
    ]
    only = ["docker.alpine"]
  }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt-get update",
      "apt-get upgrade -y",
      "addgroup --gid ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} --uid ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/bash ${local.runtime_usr} --disabled-password --gecos \"\"",
      "apt-get install -y curl gcc g++ git make libffi-dev libssl-dev libxml2-dev libxslt-dev",
      "curl -OL https://go.dev/dl/go1.18.3.linux-amd64.tar.gz",
      "tar xf go1.18.3.linux-amd64.tar.gz",
      "chown -R root:root ./go",
      "mv ./go /usr/local",
      "ln -s /usr/local/go/bin/go /usr/local/bin/go",
      "ln -s /usr/local/go/bin/gofmt /usr/local/bin/gofmt",
      "echo 'apt-get install -y $@' > /usr/local/bin/pkg-add",
      "echo 'apt-get update' > /usr/local/bin/pkg-update"
    ]
    only = ["docker.debian"]
  }

  provisioner "shell" {
    inline = [
      "cd /tmp",
      "chmod +x /usr/local/bin/pkg-add",
      "chmod +x /usr/local/bin/pkg-update",
      "pip install pip --upgrade",
      "rm -rf /tmp/*"
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = var.docker_repository
      tags        = [var.docker_tag]
    }

    post-processor "docker-push" {}
  }
}

