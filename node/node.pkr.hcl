

locals {
  alpine_packages = [
    "curl",
    "git",
    "gcc",
    "g++",
    "libressl",
    "make",
    "python3",
    "libsass-dev"
  ]
  debian_packages = [
    "curl",
    "git",
    "gcc",
    "g++",
    "openssl",
    "make",
    "python",
    "libsass-dev"
  ]
  extra_commands = [
    "npm install -g node-gyp"
  ]
}
