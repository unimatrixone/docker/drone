variable "docker_base_image" { type=string }
variable "docker_tag" { type=string }
variable "docker_repository" { type=string }


build {
  name = "gitlab-runner"

  source "source.docker.alpine" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
      "ENTRYPOINT [\"\"]",
      "CMD []"
    ])
  }

  source "source.docker.debian" {
    image   = var.docker_base_image
    commit  = true
    changes = concat([
      "LABEL maintainer=${local.maintainer}",
      "LABEL version=latest",
      "ENTRYPOINT [\"\"]",
      "CMD []"
    ])
  }

  provisioner "shell" {
    inline = [
      "apk -U upgrade",
      "apk -v cache clean || true",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add ${join(" ", local.alpine_packages)}"
    ]
    only = ["docker.alpine"]
  }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt-get update",
      "apt-get upgrade -y",
      "apt-get install -y ${join(" ", local.debian_packages)}"
    ]
    only = ["docker.debian"]
  }

  provisioner "shell" {
    inline = local.extra_commands
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = var.docker_repository
      tags        = [var.docker_tag]
    }

    post-processor "docker-push" {}
  }
}


