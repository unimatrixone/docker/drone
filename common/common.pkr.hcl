

locals {
  app_secdir   = "/var/run/secrets/unimatrixone.io"
  runtime_usr  = "drone"
  runtime_grp  = "drones"
  runtime_uid  = 1000
  runtime_gid  = 1000
  runtime_home = "/opt/app"
  maintainer   = "oss@unimatrixone.io"
  docker_env   = [
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}",
    "ENV APP_PKIDIR ${local.runtime_home}/pki",
    "ENV APP_RUNDIR ${local.runtime_home}",
    "ENV APP_LIBDIR ${local.runtime_home}/lib",
    "ENV APP_SECDIR ${local.app_secdir}",
    "ENV RUNTIME_USR ${local.runtime_usr}",
    "ENV RUNTIME_UID ${local.runtime_uid}",
    "ENV RUNTIME_GRP ${local.runtime_grp}",
    "ENV RUNTIME_GID ${local.runtime_gid}",
    "ENV RUNTIME_HOME ${local.runtime_home}",
  ]
}
