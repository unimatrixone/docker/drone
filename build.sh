#/bin/sh
IMAGE_VARIANT=$1
PYTHON_VERSION=$2
PYTHON_BASE_TAG=$3
OS_RELEASE_ID=$4
OS_RELEASE_VERSION=$5
TARGET_TAG=${OS_RELEASE_ID}${OS_RELEASE_VERSION}
packer build\
  -var docker_base_image=python:${PYTHON_VERSION}-${PYTHON_BASE_TAG}\
  -var docker_tag=${PYTHON_VERSION}-${TARGET_TAG}\
  -var docker_repository=registry.gitlab.com/unimatrixone/docker/drone/${IMAGE_VARIANT}/python\
  -only=$IMAGE_VARIANT.docker.${OS_RELEASE_ID} .


# ./build.sh build 3.9 alpine3.12 alpine 3.12 3.9-alpine3.12
