#/bin/sh
IMAGE_REPO=$1
IMAGE_VARIANT=$2
IMAGE_VERSION=$3
IMAGE_BASE_TAG=$4
OS_RELEASE_ID=$5
OS_RELEASE_VERSION=$6
TARGET_TAG=${OS_RELEASE_ID}${OS_RELEASE_VERSION}
cp common/common.pkr.hcl $IMAGE_REPO/
cp common/sources.pkr.hcl $IMAGE_REPO/
packer build\
  -var docker_base_image=${IMAGE_REPO}:${IMAGE_VERSION}-${IMAGE_BASE_TAG}\
  -var docker_tag=${IMAGE_VERSION}-${TARGET_TAG}\
  -var docker_repository=registry.gitlab.com/unimatrixone/docker/drone/${IMAGE_VARIANT}/${IMAGE_REPO}\
  -only=$IMAGE_VARIANT.docker.${OS_RELEASE_ID}\
  $IMAGE_REPO/


# ./build.sh python build 3.9 alpine3.12 alpine 3.12 3.9-alpine3.12
